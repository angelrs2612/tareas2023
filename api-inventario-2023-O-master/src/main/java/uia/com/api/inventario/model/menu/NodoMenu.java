package uia.com.api.inventario.model.menu;

import jakarta.validation.Validator;
import uia.com.api.inventario.repository.*;

public class NodoMenu extends Nodo{

    public NodoMenu(String titulo, String subTitulo, String[] opciones, String seleccion, String subSeleccion, int tipo,
                    PartidaRepository repositoryPartidas,
                    LoteRepository repositoryLote,
                    ItemRepository repositoryItem,
                    SubpartidaRepository repositorySubpartidas,
                    CategoriaRepository repositoryCategoria)
    {


        super(titulo, subTitulo, opciones, seleccion, subSeleccion, tipo, repositoryPartidas,
                repositoryLote,
                repositoryItem,
                repositorySubpartidas,
                repositoryCategoria);


    }
}
