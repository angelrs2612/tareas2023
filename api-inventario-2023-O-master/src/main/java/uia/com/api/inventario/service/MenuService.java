package uia.com.api.inventario.service;

import jakarta.validation.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uia.com.api.inventario.model.ControlId;
import uia.com.api.inventario.model.Partida;
import uia.com.api.inventario.model.menu.NodoMenu;
import uia.com.api.inventario.repository.*;

import java.util.ArrayList;


@Service
public class MenuService {
    PartidaRepository repositoryPartidas;
    SubpartidaRepository repositorySubpartidas;
    LoteRepository repositoryLote;
    ItemRepository repositoryItem;
    CategoriaRepository repositoryCategoria;
    ControlId idControl;
    ControlId idControlDTO;

    Validator validator;

    @Autowired
    public MenuService(PartidaRepository repositoryPartidas,
                       LoteRepository repositoryLote,
                       ItemRepository repositoryItem,
                       SubpartidaRepository repositorySubpartida,
                       CategoriaRepository repositoryCategoria,
                       Validator validator) {
        this.repositoryPartidas = repositoryPartidas;
        this.repositoryLote = repositoryLote;
        this.repositoryItem = repositoryItem;
        this.repositorySubpartidas = repositorySubpartida;
        this.repositoryCategoria = repositoryCategoria;
        this.validator = validator;
    }

    public void menus() {
        System.out.println("Hola, iniciando prueba de concepto de menu, buena suerte");

        ArrayList<String> listaItems = reporteNivelInventario();

        String[] opcionesMenuMain = {"Salir", "PeticionesSolicitudCompra", "SolicitudesCompra", "OrdenesCompras"};
        String[] opcionesMenuPeticionesSolictitudCompra = {"Salir", "Nuevo", "Modificar", "Borrar"};
        String[] opcionesMenuNuevoPeticionSolicitudCompra = {"Salir", "Nombre", "Cantidad"};

        NodoMenu menuPrincipal = new NodoMenu("Principal", "Introduzca la opción deseada ?:", opcionesMenuMain, "", "", 0, repositoryPartidas,
                repositoryLote,
                repositoryItem,
                repositorySubpartidas,
                repositoryCategoria);

        menuPrincipal.setMenu("PeticionesSolicitudCompra", "PeticionesSolicitudCompra", "Introduzca la opción deseada ?:", opcionesMenuPeticionesSolictitudCompra, "", "", 0);
        menuPrincipal.getNodoMenu("PeticionesSolicitudCompra")
                .getNodoMenu("Crear Peticion Compra")
                .setMenu("Crear Peticion Compra", "Crear Peticion Compra", "Introduzca la opción deseada ?:", opcionesMenuNuevoPeticionSolicitudCompra, "", "", 0);

        /*
        String[] opcionesMenuNuevoPeticionSolicitudCompraNombre ={"Salir", "Introduzca el nombre del contacto ?"};
        menuPrincipal.getNodoMenu("PeticionesSolictitudCompra")
                .getNodoMenu("Nuevo")
                .getNodoMenu("Nombre")
                .setMenu("Nombre", "Nombre", "Introduzca el nombre del contacto ?:",  opcionesMenuNuevoPeticionSolicitudCompraNombre, "", "", 1);
         */
        String dondeEstoy = "";
        menuPrincipal.pregunta(dondeEstoy);

    }

    public ArrayList<String> reporteNivelInventario() {
        ArrayList<Partida> listaSolicitudes = (ArrayList<Partida>) this.repositoryPartidas.findAll();
        ArrayList<String> miLista = new ArrayList<>();
        String cad;
        for (int i = 0; i < listaSolicitudes.size(); ++i) {
            cad = "numero:\t" + listaSolicitudes.get(i).getIdPartida() + "\tnombre:" + listaSolicitudes.get(i).getName() + "\tcantidad" + listaSolicitudes.get(i).getCantidad();
            miLista.add(cad);
        }

        return miLista;
    }



}
