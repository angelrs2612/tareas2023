package uia.com.api.inventario.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uia.com.api.inventario.service.MenuService;


@RestController
@RequestMapping("/Menus")
@CrossOrigin(origins = "http://localhost:4200")
public class MenuController {

    private MenuService menuService;

    @Autowired
    public MenuController(MenuService menuService) {
        this.menuService = menuService;
    }



    @GetMapping
    public ResponseEntity<String> menus()
    {
        this.menuService.menus();
        return new ResponseEntity<>("Adios Menu", HttpStatus.CREATED);
    }
}
